import React, { Component } from 'react';
import './App.css';
import Game from './components/tictactoe/tictactoe.js';
import AnalogClock from './components/analogClock/analogClock.js';

class App extends Component {
  constructor() {
    super();
    this.state = {
      currentTime: new Date(),
      message: 'Let\'s Play Tick-Tock-Toe!!!',
      turn: true,
      xSec: 0,
      xMin: 0,
      xHrs: 0,
      oSec: 0,
      oMin: 0,
      oHrs: 0,
      xAvg: 0,
      oAvg: 0,
      xArr: [] || 0,
      oArr: [] || 0,
    };
    this.handleTurn = this.handleTurn.bind(this);
  }

  handleTurn(trn) {
    this.setState({
      turn: trn,
    });
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  updateCurrentTime() {
    this.setState({
      currentTime: new Date(),
    });
  }

  updateMin(min, sec) {
    if(sec === 60) {
      return min + 1;
    }
  }

  updateHrs(hrs, min) {
    if(min === 60) {
      return hrs + 1;
    }
  }

  updateXTime() {
    if(this.state.turn) {
      this.setState({
        xSec: this.state.xSec + 1,
        xMin: this.updateMin(this.state.xMin, this.state.xSec) || 0,
        xHrs: this.updateHrs(this.state.xHrs, this.state.xMin) || 0,
      });
    }
  }

  updateOTime() {
    if(!this.state.turn) {
      this.setState({
        oSec: this.state.oSec + 1,
        oMin: this.updateMin(this.state.oMin, this.state.oSec) || 0,
        oHrs: this.updateHrs(this.state.oHrs, this.state.oMin) || 0,
      });
    }
  }

  updateAvg() {
    if(this.state.turn) {
      this.state.xArr.push(this.state.xSec);
    }
    if(!this.state.turn) {
      this.state.oArr.push(this.state.oSec);
    }
  }

  calcAvg(arr) {
    let total = 0;
    for(let i = 0; i < arr.length; i++){
      total += arr[i];
    }
    return total / arr.length;
  }

  tick() {
    this.updateCurrentTime();
    this.updateXTime();
    this.updateOTime();
    this.updateAvg();

    this.setState({
      xAvg: this.calcAvg(this.state.xArr) || 0,
      oAvg: this.calcAvg(this.state.oArr) || 0,
    });

    //reset x at 60
    // if (this.state.xSec === 60) {
    //   this.setState({
    //     xSec: 0,
    //   });
    // }

    // if (this.state.oSec === 60) {
    //   this.setState({
    //     oSec: 0,
    //   });
    // }

    // //reset o at 60
    // if (this.state.xMin === 60) {
    //   this.setState({
    //     xMin: 0,
    //   });
    // }

    // if (this.state.oMin === 60) {
    //   this.setState({
    //     oMin: 0,
    //   });
    // }
  }

  render() {
    return (
      <div className="App">
        <div id="x-clock">
        <h1>X Average Time {this.state.xAvg}</h1>
        <h1>X Total Time {this.state.xSec}</h1>
          <AnalogClock
            seconds={this.state.xSec}
            minutes={this.state.xMin}
            hours={this.state.xHrs}
          />
        </div>
        <div id="game">
          <Game
            handleTurn={this.handleTurn}
          />
        </div>
        <div id="o-clock">
        <h1>O Average Time {this.state.oAvg}</h1>
        <h1>O Total Time {this.state.oSec}</h1>
          <AnalogClock
            seconds={this.state.oSec}
            minutes={this.state.oMin}
            hours={this.state.oHrs}
          />
        </div>
      </div>
    );
  }
}

export default App;

import React, { Component } from 'react';
import './clock.css';

class Clock extends Component {
  render() {
    return (
      <div>
        <h1>{ this.props.message }</h1>
        <h2>Game Time Elapsed: { this.props.currentTime }</h2>
      </div>
    );
  }
} // end of clock class.

export default Clock;
// Implemented from The Anglea Way css clock tutorial
// https://medium.com/the-andela-way/create-a-pure-css-clock-with-svg-f123bcc41e46

import React, { Component } from 'react';
import './analogClock.css';

class AnalogClock extends Component {

  // constructor(props) {
  //   super(props);
  //   this.state = {date: new Date()}
  // }

  render(){
    return (
      <div
        style={{
          "--start-seconds": this.props.seconds,
          "--start-minutes": this.props.minutes,
          "--start-hours": this.props.hours,
        }}
      >
        <svg
          className="clock-svg"
          viewBox="0 0 40 40"
        >
          <circle cx="20" cy="20" r="19" />
          <g className="clock-marks">
            <line x1="15" y1="0" x2="16" y2="0" />
            <line x1="15" y1="0" x2="16" y2="0" />
            <line x1="15" y1="0" x2="16" y2="0" />
            <line x1="15" y1="0" x2="16" y2="0" />
            <line x1="15" y1="0" x2="16" y2="0" />
            <line x1="15" y1="0" x2="16" y2="0" />
            <line x1="15" y1="0" x2="16" y2="0" />
            <line x1="15" y1="0" x2="16" y2="0" />
            <line x1="15" y1="0" x2="16" y2="0" />
            <line x1="15" y1="0" x2="16" y2="0" />
            <line x1="15" y1="0" x2="16" y2="0" />
            <line x1="15" y1="0" x2="16" y2="0" />
          </g>
{/*          <line className="hour" x1="0" y1="0" x2="9" y2="0" />
          <line className="minute" x1="0" y1="0" x2="13" y2="0" />
*/}          <line className="seconds" x1="0" y1="0" x2="16" y2="0" />
          <circle className="pin" cx="20" cy="20" r="0.7" />
        </svg>
      </div>
    );
  }
} // end of analog clock class

export default AnalogClock;